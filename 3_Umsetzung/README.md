# Umsetzung

- Bereich: Innovation
- Semester: 5

## Lektionen

* Präsenzlektionen (Vor Ort): 40
* Präsenzlektionen (Distance Learning): 0
* Selbststudium: 10

### Fahrplan

| **Lektionen** | **Selbststudium** | **Inhalt**                             | **Kompetenzenband**    | **Tools**                                      |
|---------------|-------------------|----------------------------------------|------------------------|------------------------------------------------|
| 8             | 4                 | Überwachtes Lernen                     | a)                    | Jupyter Notebook, Scikit-learn                 |
| 7             | 3                 | Unüberwachtes Lernen                   | b)                    | Jupyter Notebook, Scikit-learn, Pandas         |
| 6             | 3                 | Bestärkendes Lernen                    | c)                    | OpenAI Gym, TensorFlow                         |
| 6             | 3                 | Neuronale Netze (NN)                   | d)                           | TensorFlow, Keras                              |
| 5             | 2                 | Large Language Models (LLMs)           | e)                             | Hugging Face, GPT-3, BERT                      |
| 4             | 2                 | Tokenization                           | f)                                   | Python, NLTK, SpaCy                            |
| 4             | 2                 | Natural Language Processing (NLP)      | g)                                   | NLTK, SpaCy, Hugging Face                      |
| 3             | 1                 | Quantencomputing                       | h)                                        | Qiskit, IBM Quantum Experience                 |
| **40**        | **20**            | **Total**                              |                                                                                       |                                                |

## Voraussetzungen

* Module Semester 1 - 4 

## Dispensation 

* Keine

## Methoden

Vorträge über Innovative Methoden umgesetzt in Praktischen Laborübungen mit Coaching durch Lehrperson

## Schlüsselbegriffe

* Maschinelles Lernen (ML), Überwachtes Lernen, Unüberwachtes Lernen, Bestärkendes Lernen (Reinforcement Learning)
* KI, Neuronale Netze (NN), Large Language Models (LLMs), Tokenization, Natural Language Processing (NLP)    
* Quantencomputing

## Lerninhalte

 * Entwicklung der Fähigkeit, innovative Technologien unabhängig vom spezifischen Bereich zu erkennen, zu bewerten und deren Einsatzpotenzial für verschiedene Industrien und Anwendungsfälle zu bestimmen.
 * Durchführung Proof of Concept (PoC)-Projekte, inklusive Planung, Implementierung und Evaluation, um den Mehrwert und die Machbarkeit neuer Technologien zu verifizieren.
 * Verstehen und Anwenden von Best Practices im Innovationsmanagement, um einen strukturierten Rahmen für die Erkundung und Integration neuer Technologien in bestehende Systeme und Prozesse zu schaffen
 * Auswirkungen neuer Technologien hinsichtlich Datenschutz, Sicherheit und Ethik zu analysieren und entsprechende Risikobewertungen und Strategien zu entwickeln

## Übungen und Praxis

Die Themen und Projekte können beliebige innovative Felder beinhalten (Von KI, Quantencomputing, automatische Entscheidungssysteme, neue Cloud-Services, Machinelearning, usw.)

## Lehr- und Lernformen

Vorträge von Gastdozenten, Gespräche und Diskussionen, Workshop, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

* wird vorgängig bekanntgegeben.








