# Handlungsziele und Handlungssituationen 

### **1. Maschinelles Lernen (ML)**

#### **1.1 Überwachtes Lernen**

Der Studierende versteht die grundlegenden Konzepte des überwachten Lernens und kann ein einfaches Modell basierend auf gelabelten Daten trainieren.

**Typische Handlungssituation:**  
Ein Unternehmen möchte die Kaufgewohnheiten seiner Kunden vorhersagen. Der Studierende wird beauftragt, ein Modell für die Vorhersage zu entwickeln. Dazu verwendet er gelabelte historische Daten und trainiert ein **überwachtes Lernmodell**, das darauf basiert, Kundenverhalten auf zukünftige Käufe zu übertragen.

---

#### **1.2 Unüberwachtes Lernen**

Der Studierende versteht die Grundlagen des unüberwachten Lernens und kann einfache Cluster- oder Assoziationsmodelle auf unglabelten Daten anwenden.

**Typische Handlungssituation:**  
Ein Unternehmen möchte Kunden in verschiedene Segmente einteilen, hat aber keine vorgefertigten Labels für die Daten. Der Studierende wird beauftragt, unüberwachtes Lernen anzuwenden, um ein **Clustering-Modell** zu entwickeln, das die Kunden basierend auf Ähnlichkeiten automatisch in Gruppen segmentiert.

---

#### **1.3 Bestärkendes Lernen (Reinforcement Learning)**

Der Studierende kann die Grundprinzipien des bestärkenden Lernens anwenden, um einfache Belohnungssysteme zu entwickeln und Modelle zu trainieren, die durch Interaktionen mit einer Umgebung lernen.

**Typische Handlungssituation:**  
Ein Unternehmen entwickelt einen selbstfahrenden Roboter und benötigt ein Steuerungssystem, das durch Versuch und Irrtum lernt. Der Studierende wird beauftragt, ein **Reinforcement Learning**-Modell zu erstellen, bei dem der Roboter durch Belohnungen lernt, Hindernissen auszuweichen und effizient zu navigieren.

---

### **2. Künstliche Intelligenz (KI)**

#### **2.1 Neuronale Netze (NN)**

Der Studierende versteht die Architektur und Funktionsweise von neuronalen Netzen und kann ein einfaches **feed-forward neuronales Netz** für Klassifikationsaufgaben implementieren.

**Typische Handlungssituation:**  
Ein Unternehmen möchte handschriftliche Ziffern automatisch erkennen. Der Studierende wird beauftragt, ein **neuronales Netz** zu erstellen, das die Bilddaten der handschriftlichen Ziffern analysiert und sie den korrekten Klassen (0-9) zuordnet.

---

#### **2.2 Large Language Models (LLMs)**

Der Studierende versteht die Grundlagen grosser Sprachmodelle und kann ein vortrainiertes LLM anwenden, um einfache Textaufgaben wie Textvervollständigung oder Textklassifikation zu lösen.

**Typische Handlungssituation:**  
Ein Unternehmen möchte seinen Kundendienst automatisieren und ein Chatbot-System implementieren. Der Studierende wird beauftragt, ein vortrainiertes **Large Language Model** wie GPT zu verwenden, um den Chatbot zu entwickeln, der in der Lage ist, Kundenanfragen zu beantworten und dynamisch auf Texteingaben zu reagieren.

---

#### **2.3 Tokenization**

Der Studierende versteht das Konzept der Tokenization und kann Textdaten in Token zerlegen, um sie für ein Sprachmodell vorzubereiten.

**Typische Handlungssituation:**  
Ein Unternehmen möchte grosse Textdatenmengen für ein Sprachmodell aufbereiten. Der Studierende wird beauftragt, die Daten durch **Tokenization** zu zerlegen und sicherzustellen, dass der Text korrekt segmentiert wird, um als Input für ein Sprachmodell verwendet zu werden.

---

#### **2.4 Natural Language Processing (NLP)**

Der Studierende kann die grundlegenden Techniken des NLP anwenden, um Textdaten zu analysieren, zu verarbeiten und Aufgaben wie Textklassifikation, Named Entity Recognition oder Sentiment-Analyse durchzuführen.

**Typische Handlungssituation:**  
Ein Unternehmen möchte Kundenrezensionen analysieren, um das allgemeine Feedback zu seinen Produkten zu verstehen. Der Studierende wird beauftragt, **NLP-Techniken** zu verwenden, um eine Sentiment-Analyse durchzuführen und die Kundenmeinungen in positive, negative oder neutrale Kategorien einzuordnen.

---

### **3. Quantencomputing**

Der Studierende versteht die grundlegenden Prinzipien des Quantencomputings und kann einfache Konzepte wie **Superposition** und **Qubits** erklären.

**Typische Handlungssituation:**  
Ein Forschungsinstitut interessiert sich für die Grundlagen des Quantencomputings und möchte verstehen, wie es in der Praxis angewendet werden könnte. Der Studierende wird beauftragt, eine einfache **Quantencomputing-Simulation** zu verwenden, um die Funktionsweise von Qubits und Superposition zu demonstrieren und grundlegende Konzepte des Quantencomputings vorzustellen.
